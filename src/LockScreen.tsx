import { useEffect, useState } from "react";
import { ethers } from "ethers";
import { Signer } from "ethers";
import { Lock__factory } from "../types/ethers-contracts";

export default function LockScreen() {
  const now = Math.round(Date.now() / 1000);
  const MINUTES = 60;
  const unlockTime = now + 2 * MINUTES;
  const lockedAmount = ethers.parseEther("0.001");

  const [myAccount, setMyAccount] = useState<Signer | null>(null);
  const [myAccountAddress, setMyAccountAddress] = useState<string | null>(null);
  const [lockAddress, setLockAddress] = useState("");

  useEffect(() => {
    let provider;
    console.log("test");

    if ((window as any).ethereum == null) {
      console.log("no provider! quit...");
      return;
    }
    console.log("provider", (window as any).ethereum);

    provider = new ethers.BrowserProvider((window as any).ethereum);
    provider.getSigner().then((signer) => {
      setMyAccount(signer!);
      signer!.getAddress().then((address) => {
        setMyAccountAddress(address);
        console.log("signer adress", address);
      });
    });
  }, []);

  return (
    <>
      <h1>Welcome!</h1>
      {!myAccountAddress && <div>Please connect your wallet...</div>}
      {!!myAccountAddress && (
        <div>
          <div>
            <p>My Web3 current account: {myAccountAddress}</p>
          </div>

          {
            <div>
              {/* <div>
                <div>You will play against : {opponentName} </div>
                <button onClick={() => setOpponentName("")}>Change</button>
              </div> */}
              <div>
                <h2>What do you want to do?</h2>
                <div style={{ border: "1px solid white", padding: "10px" }}>
                  <h2>Create a new lock?</h2>

                  <button
                    disabled={!myAccount}
                    onClick={async () => {
                      const factory = new Lock__factory(
                        Lock__factory.abi,
                        Lock__factory.bytecode,
                        myAccount!
                      );
                      console.log("myAdress", myAccountAddress);
                      const pendingLock = await factory.deploy(unlockTime, {
                        value: lockedAmount,
                      });
                      const newLock = await pendingLock.waitForDeployment();
                      console.log(
                        "new lock adress:",
                        await newLock.getAddress()
                      );
                    }}
                  >
                    New Lock
                  </button>
                </div>
              </div>
              <div>
                <h2>OR</h2>
              </div>
              <div style={{ border: "1px solid white", padding: "10px" }}>
                <h2>Use an existing lock?</h2>
                <div>
                  <label htmlFor="lockAddress">Lock address: </label>
                  <input
                    style={{ width: "400px" }}
                    type="text"
                    value={lockAddress}
                    onChange={(e) => setLockAddress(e.target.value)}
                  />
                </div>
                <button
                  disabled={!myAccount || !lockAddress}
                  onClick={async () => {
                    const lock = Lock__factory.connect(lockAddress,myAccount)
                    console.log("existing lock adress:",await lock.getAddress());
                    try {
                      const response = await lock.withdraw();
                      console.log("response",response);
                    } catch (error : any) {
                      console.log(error.reason);
                      
                    }  
                  }}
                >
                  Withdraw from lock
                </button>
              </div>
            </div>
          }
        </div>
      )}
    </>
  );
}
