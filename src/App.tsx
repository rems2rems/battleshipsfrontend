import { useState } from "react";
import "./App.css";
import PlaceBoats from "./PlaceBoats";
import PlayGame from "./PlayGame";
import { Boat, BoatType } from "./model/Battleship";
import LockScreen from "./LockScreen";

function App() {
  const [lock, setLock] = useState<Lock | null>(null);
  const [opponentName, setOpponentName] = useState("");
  const [myBoats, setMyBoats] = useState<Map<BoatType, Boat>>(new Map());

  return (
    <>
      {!lock && <LockScreen />}
      {lock && myBoats.size < 5 && (
        <PlaceBoats
          onDone={(boats) => {
            setMyBoats(boats);
          }}
        />
      )}
      {lock && myBoats.size === 5 && (
        <PlayGame myBoats={myBoats} opponentName={opponentName} />
      )}
      {/* </GameProvider> */}
    </>
  );
}

export default App;
